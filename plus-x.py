import os
import sys
import time
import signal
import zipfile

class PlusX():
	executableMask = 0b10000001111111011000000000000000
	filename = ""
	archive = None
	modifiedCount = 0
	fileList = []

	applicationName = "plus-x"
	applicationVersion = "v0.2"
	applicationDescription = "A tool for managing UNIX executable\npermissions on zipped applications."
	applicationWebsiteURI = "https://cheeseness.itch.io/plus-x"
	applicationSourceURI = "https://gitlab.com/Cheeseness/plus-x"
	applicationCopyright = "Copyright (c) 2021 Josh \"Cheeseness\" Bush"
	applicationLogo = ""

	applicationInstructions = "1. Open a Zip archive by clicking on the Open archive button or selecting Open from the File menu.\n\n2. Locate binaries within the archive that should be executable and click in the executable column.\n\n3. Click the Apply button or select Apply from the File menu to update the archive contents.\n\n4. Ship your application with peace of mind that your Linux users will be able to run it without fiddling with permissions."

	def displayVersion(self):
		print("Running %s %s" % (self.applicationName, self.applicationVersion))


	def openArchive(self, path = ""):
		if path == () or path == "" or path == None:
			return False
		print("Opening archive %s" % path)
		self.filename = path
		try:
			self.archive = zipfile.ZipFile(self.filename, 'r', zipfile.ZIP_DEFLATED)
			self.updateFileList()
		except:
			print("Something went wrong while opening archive.")
			return False

		return True

	def updateFileList(self):
		self.fileList = []
		self.modifiedCount = 0

		index = 0
		for archiveFile in self.archive.infolist():
			if not archiveFile.filename.endswith("/"):
				pathChunks = archiveFile.filename.rsplit("/", 1)
				if len(pathChunks) == 1:
					pathChunks.insert(0, "")
				executable = (archiveFile.external_attr & self.executableMask) == self.executableMask
				self.fileList.append([archiveFile.filename, pathChunks[1], pathChunks[0] + "/", executable, executable, False, index])
				index += 1


	def resetFiles(self):
		self.modifiedCount = 0
		for archiveFile in self.fileList:
			archiveFile[3] = archiveFile[4]
			archiveFile[5] = False


	def toggleFile(self, fileIndex = -1):
		if fileIndex >= 0 and fileIndex < len(self.fileList):
			self.fileList[fileIndex][3] = not self.fileList[fileIndex][3]

			if self.fileList[fileIndex][3] != self.fileList[fileIndex][4]:
				self.fileList[fileIndex][5] = True
				self.modifiedCount += 1
			else:
				self.fileList[fileIndex][5] = False
				self.modifiedCount -= 1
		else:
			print("File index out of bounds?", fileIndex)


	def updateArchive(self, cb = None):
		tempFilename = self.filename[:-4] + "_updated.zip"

		writeArchive = zipfile.ZipFile(tempFilename, "w", zipfile.ZIP_DEFLATED)
		writeCount = 0
		freshFileList = self.archive.infolist()
		processedFiles = 0

		if cb != None:
			cb(0.0)

		updatedFiles = []
		for archiveFile in self.fileList:
			if archiveFile[5] == True:
				updatedFiles.append(archiveFile)

		sys.stdout.write("Updating permissions on %d of %d files" % (len(updatedFiles), len(freshFileList)))
		sys.stdout.flush()

		for archiveFile in freshFileList:
			internalFile = self.archive.open(archiveFile.filename)
			byteFriend = internalFile.read()
			internalFile.close()
			infoFriend = self.archive.getinfo(archiveFile.filename)

			if cb != None:
				cb(float(processedFiles) / float(len(freshFileList)))

			for updatedFile in updatedFiles:
				if archiveFile.filename == updatedFile[0]:
					if updatedFile[3] == True:
						infoFriend.external_attr = infoFriend.external_attr | self.executableMask
					else:
						infoFriend.external_attr = infoFriend.external_attr & ~self.executableMask
					infoFriend.date_time = time.localtime()
					writeCount += 1
					break
			writeArchive.writestr(infoFriend, byteFriend)
			processedFiles += 1

			sys.stdout.write(".")
			sys.stdout.flush()

			if cb != None:
				cb(float(processedFiles) / float(len(freshFileList)))

		writeArchive.close()
		self.archive.close()
		os.remove(self.filename)
		os.rename(tempFilename, self.filename)

		if writeCount == len(updatedFiles):
			print(" Done!")
		else:
			print("Something went wrong. %d files were modified." % writeCount)

		self.archive = zipfile.ZipFile(self.filename, 'r', zipfile.ZIP_DEFLATED)
		self.updateFileList()


	def teardown(self):
		print("Unloading %s" % self.filename)
		if self.modifiedCount > 0:
			print("Discarding unapplied changes!")
			self.modifiedCount = 0

		if self.archive != None:
			self.archive.close()


class PlusXCL:
	plusX = None

	def __init__(self, f = "", p = PlusX()):
		self.plusX = p
		self.plusX.displayVersion()
		if f == None or f == "":
			print("No archive specified. First non-option argument needs to be a valid ZIP archive.\nRun with --help for instructions.")
			p.teardown()
			sys.exit(1)
		if not self.plusX.openArchive(f):
			print("%s may not be a valid ZIP archive." % f)
			p.teardown()
			sys.exit(1)

		add = False
		if "+x" in sys.argv:
			add = True
			sys.argv.remove("+x")
		if "-x" in sys.argv:
			sys.argv.remove("-x")
		sys.argv.remove(f)
		filenames = sys.argv[1:]
		for i in range(len(filenames)):
			filenames[i] = filenames[i].replace("\\", "/")
		fileCount = len(filenames)
		if fileCount == 0:
			print("No files specified. Nothing to do.")
			p.teardown()
			sys.exit(1)
		for archiveFile in self.plusX.fileList:
			if archiveFile[0] in filenames:
				filenames.remove(archiveFile[0])
				if add == archiveFile[3]:
					print("%s is already %sexectuable" % (archiveFile[0], ("" if archiveFile[3] else "not ")))
				else:
					self.plusX.toggleFile(archiveFile[6])
		if len(filenames) == 0:
			self.plusX.updateArchive()
			sys.exit(0)
		else:
			print("The following files were not found in the archive. Changes have not been applied.")
			for fname in filenames:
				print(fname)
			sys.exit(1)


class PlusXCLI:
	plusX = None
	states = {}
	currentState = "m"

	def __init__(self, f = "", p = PlusX()):
		self.states = {
				"m": {"func": self.doMenu},
				"?": {"func": self.doHelp, "desc": "Display this text"},
				"l": {"func": self.doList, "desc": "List all files in the archive"},
				"s": {"func": self.doSummary, "desc": "Summarise pending changes"},
				"+": {"func": self.doAdd, "desc": "Add executable permissions to files"},
				"-": {"func": self.doRemove, "desc": "Remove executable permissions from files"},
				"c": {"func": self.doApply, "desc": "Confirm and apply changes"},
				"q": {"func": self.doQuit, "desc": "Quit plus-x"}
			}
		self.plusX = p
		self.plusX.displayVersion()

		if f == None or f == "":
			print("No archive specified. First non-option argument needs to be a valid ZIP archive.\nRun with --help for instructions.")
			return
		if not self.plusX.openArchive(f):
			print("%s may not be a valid ZIP archive." % f)
			return

		self.doHelp()
		self.processCLI()


	def getInput(self, string):
		if sys.version_info[0] < 3:
			return raw_input(string)
		else:
			return input(string)


	def doHelp(self):
		print("")
		for state in self.states:
			if "desc" in self.states[state]:
				print("%s: %s" % (state.upper(), self.states[state]["desc"]))
		self.currentState = "m"


	def doMenu(self):
		temp = list(self.states.keys())
		temp.remove("m")
		print("\nAction: [%s]" % "/".join(temp))
		userInput = self.getInput("> ").lower()
		if userInput in self.states.keys():
			self.currentState = userInput
		elif userInput != "":
			self.doFallback()


	def doList(self):
		print("\nListing files in : %s" % self.plusX.filename)
		self.listFiles()
		self.currentState = "m"


	def doSummary(self):
		print("\nListing unapplied changes:")
		self.listFiles(True)
		self.currentState = "m"


	def doAdd(self):
		print("\nEnter the path of a file to give executable permissions to, or press Enter to return")
		#TODO: Support tab completion?
		userInput = self.getInput("Add > ")
		if userInput == "":
			self.currentState = "m"
			return
		if userInput == "-":
			print("Switching to remove...")
			self.currentState = userInput
			return

		index =  self.getFileIndex(userInput)
		if index >= 0:
			if self.plusX.fileList[index][3]:
				print("File is already marked as executable.")
			else:
				self.plusX.toggleFile(index)
		else:
			print("File not found within archive.")


	def doRemove(self):
		print("\nEnter the path of a file to remove executable permissions from, or press Enter to return")
		#TODO: Support tab completion?
		userInput = self.getInput("Remove > ")
		if userInput == "":
			self.currentState = "m"
			return
		if userInput == "+":
			print("Switching to add...")
			self.currentState = userInput
			return

		index = self.getFileIndex(userInput)
		if index >= 0:
			if not self.plusX.fileList[index][3]:
				print("File is already marked as not executable.")
			else:
				self.plusX.toggleFile(index)
		else:
			print("File not found within archive.")


	def doApply(self):
		self.plusX.updateArchive()
		self.currentState = "m"


	def doQuit(self):
		if self.plusX.modifiedCount > 0:
			print("Unapplied changes will be lost. Are you sure to quit? (y/n)")
			if self.getInput("> ").lower() != "y":
				self.currentState = "m"
				return
		self.currentState = "quit"
		self.plusX.teardown()
		print("Quitting!")


	def doFallback(self):
		print("Unknown command")
		self.currentState = "m"


	def processCLI(self):
		while True:
			for state in self.states:
				if self.currentState == state:
					self.states[state]["func"]()
					break
			if self.currentState == "quit":
				break


	def listFiles(self, limitToChanged = False, paginate = True, exclude = ""):
		count = 0
		unsavedCount = 0
		for archiveFile in self.plusX.fileList:
			if not limitToChanged or (limitToChanged and archiveFile[5]):
				count += 1
				print("  %s (%s%s)" % (archiveFile[0], ("executable" if archiveFile[3] else "not executable"), ("*" if archiveFile[5] else "")))
		if count == 0:
			print("  None")
		if self.plusX.modifiedCount > 0:
			print("* unsaved changes")


	def getFileIndex(self, filename = ""):
		for archiveFile in self.plusX.fileList:
			if archiveFile[0] == filename:
				return archiveFile[6]
		return -1



class PlusXGTKUI:
	try:
		import gi
		gi.require_version('Gtk', '3.0')
		from gi.repository import Gtk
		from gi.repository import Gdk
	except:
		print("GTK UI will not be available (requires pygobject module)")

	builder = None
	listStore = None
	plusX = None


	def __init__(self, f = "", p = PlusX()):
		self.plusX = p
		self.plusX.displayVersion()

		print("Initialising GTK UI")

		self.builder = self.Gtk.Builder()
		self.builder.add_from_file(gladeFilePath)

		self.builder.get_object("windowAbout").set_transient_for(self.builder.get_object("windowMain"))
		self.builder.get_object("windowProgress").set_transient_for(self.builder.get_object("windowMain"))


		self.listStore = self.builder.get_object("liststoreContents")
		self.listStore.clear()
		self.setApplyResetEnabled(False)
		self.builder.get_object("buttonAboutClose").connect("clicked", self.onCloseAbout)

		self.builder.get_object("labelAppName").set_text(self.plusX.applicationName + " " + self.plusX.applicationVersion)
		self.builder.get_object("labelAppDescription").set_text(self.plusX.applicationDescription)
		self.builder.get_object("linkWebsite").set_uri(self.plusX.applicationWebsiteURI)
		self.builder.get_object("linkRepo").set_uri(self.plusX.applicationSourceURI)
		self.builder.get_object("labelCopyright").set_text(self.plusX.applicationCopyright)

		self.builder.get_object("labelInstructions").set_text(self.plusX.applicationInstructions)

		self.builder.get_object("windowMain").connect("delete_event", self.onDestroy)
		self.builder.get_object("windowMain").show_all()

		#Yay, Glade doesn't know how to make these
		fileFilter = self.Gtk.FileFilter()
		fileFilter.set_name("Zip Archives")
		fileFilter.add_pattern("*.zip")
		self.builder.get_object("filechooserArchive").add_filter(fileFilter)

		self.builder.connect_signals(self)

		if f != "":
			self.onArchiveSelect(self.builder.get_object("filechooserArchive"), f)

		self.Gtk.main()

	def onDestroy(self, *args):
		if self.plusX.modifiedCount > 0:
			dialog = self.Gtk.Dialog(title = "Unapplied changes", transient_for = self.builder.get_object("windowMain"))
			dialog.set_modal(True)
			dialog.add_buttons(self.Gtk.STOCK_NO, self.Gtk.ResponseType.NO, self.Gtk.STOCK_YES, self.Gtk.ResponseType.YES)
			message = self.Gtk.Label(label = "Unapplied changes will be lost. Are you sure to quit?")
			message.set_margin_top(5)
			message.set_margin_bottom(10)
			message.set_margin_left(5)
			message.set_margin_right(5)
			dialog.get_content_area().add(message)
			dialog.show_all()
			response = dialog.run()
			dialog.destroy()
			if response == self.Gtk.ResponseType.NO:
				return True

		self.plusX.teardown()
		self.Gtk.main_quit()


	def onShowAbout(self, widget):
		self.builder.get_object("windowAbout").show_all()


	def onCloseAbout(self, widget):
		self.builder.get_object("windowAbout").hide()


	def onShowInstructions(self, widget):
		self.builder.get_object("windowInstructions").show_all()


	def onCloseInstructions(self, widget):
		self.builder.get_object("windowInstructions").hide()


	def onOpenArchive(self, widget):
		#TODO: Make this work
		chooser = self.builder.get_object("filechooserArchive")
		chooser.do_button_press_event(chooser, self.Gdk.EventButton())


	def onArchiveSelect(self, widget, filename = ""):
		if filename == "":
			filename = widget.get_filename()
		if self.plusX.openArchive(filename):
			self.populateList()
			self.updateUIStatus()


	def setApplyResetEnabled(self, enabled):
		self.builder.get_object("menuApply").set_sensitive(enabled)
		self.builder.get_object("menuReset").set_sensitive(enabled)
		self.builder.get_object("buttonApply").set_sensitive(enabled)
		self.builder.get_object("buttonReset").set_sensitive(enabled)


	def updateUIStatus(self):
		self.builder.get_object("labelModifiedCount").set_text(str(self.plusX.modifiedCount))
		if self.plusX.modifiedCount > 0:
			self.setApplyResetEnabled(True)
		else:
			self.setApplyResetEnabled(False)


	def populateList(self):
		self.listStore.clear()
		for compressedFile in self.plusX.fileList:
			self.listStore.append((compressedFile[1], compressedFile[2], compressedFile[3], compressedFile[4], False, compressedFile[6]))


	def onFileExecutableClick(self, cell, row):
		if row != None:
			it = self.listStore.get_iter(row)
			self.plusX.toggleFile(self.listStore[it][5])
			self.listStore[it][2] = not self.listStore[it][2]
			if self.listStore[it][2] != self.listStore[it][3]:
				self.listStore[it][4] = True
			else:
				self.listStore[it][4] = False
			self.updateUIStatus()

	def onResetFiles(self, _button):
		it = self.listStore.get_iter_first()
		while it != None:
			self.listStore[it][2] = self.listStore[it][3]
			self.listStore[it][4] = False
			it = self.listStore.iter_next(it)
		self.plusX.resetFiles()
		self.updateUIStatus()


	def onApplyChanges(self, _button):
		progressIndicator = self.builder.get_object("progressbar")
		progressWindow = self.builder.get_object("windowProgress")
		progressWindow.show_all()
		progressIndicator.set_fraction(0.0)
		while self.Gtk.events_pending():
			self.Gtk.main_iteration()
		self.plusX.updateArchive(self.updateProgress)
		progressWindow.hide()
		self.populateList()
		self.updateUIStatus()


	def updateProgress(self, progress):
		self.builder.get_object("progressbar").set_fraction(progress)
		while self.Gtk.events_pending():
			self.Gtk.main_iteration()



class PlusXTKUI:
	try:
		import tkinter as tk
		from tkinter import ttk
		from tkinter import filedialog
		from tkinter import messagebox
	except:
		if sys.version_info[0] >= 3:
			print("Tcl/Tk GUI will not be available in Python 3.x (requires tkinter module)")
		try:
			import Tkinter as tk
			import ttk
			import tkFileDialog as filedialog
			import tkMessageBox as messagebox
		except:
			if sys.version_info[0] < 3:
				print("Tcl/Tk GUI will not be available in Python 2.7 (requires Tkinter module)")

	windowMain = None
	windowProgress = None
	progressBar = None
	menuFile = None
	buttonOpen = None
	buttonApply = None
	buttonReset = None
	labelModifiedCount = None
	treeView = None
	progressValue = 0
	plusX = None


	def __init__(self, f = "", p = PlusX()):
		self.plusX = p
		self.plusX.displayVersion()

		print("Initialising Tcl/Tk UI")
		self.windowMain = self.tk.Tk(className = "plus-x")
		self.windowMain.geometry("600x300")
		self.windowMain.title("plus-x")
		self.windowMain.protocol('WM_DELETE_WINDOW', self.onUIDestroy)

		menuFriend = self.tk.Menu(self.windowMain)
		self.windowMain.config(menu = menuFriend)

		self.menuFile = self.tk.Menu(menuFriend, tearoff=0)
		self.menuFile.add_command(label = "Open", command = self.onOpenClick)
		self.menuFile.add_command(label = "Reset", command = self.onResetClick)
		self.menuFile.add_command(label = "Apply", command = self.onApplyClick)
		self.menuFile.add_separator()
		self.menuFile.add_command(label = "Quit", command = self.onUIDestroy)
		menuFriend.add_cascade(label = "File", menu = self.menuFile)

		menuHelp = self.tk.Menu(menuFriend, tearoff=0)
		menuHelp.add_command(label = "Instructions", command = self.onInstructionsClick)
		menuHelp.add_separator()
		menuHelp.add_command(label = "About", command = self.onAboutClick)
		menuFriend.add_cascade(label = "Help", menu = menuHelp)

		self.buttonOpen = self.tk.Button(text="Open archive", anchor = "w", command = self.onOpenClick)
		self.buttonOpen.pack(fill="both")

		self.treeView = self.ttk.Treeview(self.windowMain, columns = ("Filename", "Path", "Executable", "Modified"), selectmode = "none")
		self.treeView.tag_configure("bold", font = ('Calibri', 11,'bold'))
		self.treeView.tag_configure("default", font = ('Calibri', 11,'normal'))
		self.treeView.tag_configure("rowodd", background = "#efefef")
		self.treeView.tag_configure("roweven", background = "#ffffff")
		self.treeView.column("#0", width = 0, stretch = self.tk.NO)
		self.treeView.column("Executable", width = 100)
		self.treeView.column("Modified", width = 0, stretch = self.tk.NO)
		self.treeView.column("Filename", stretch = self.tk.YES)
		self.treeView.column("Path", stretch = self.tk.YES)
		self.treeView.heading("Filename", text = "Filename")
		self.treeView.heading("Path", text = "Path")
		self.treeView.heading("Executable", text = "Executable")
		self.treeView.heading("Modified", text = "Modified")
		self.treeView.pack(fill="both", expand = True)
		self.treeView.bind("<Button-1>", self.onTreeViewClick)


		self.labelModifiedCount = self.tk.Label(text="Pending file modifications: 0")
		self.labelModifiedCount.pack(side = "left")

		self.buttonApply = self.tk.Button(text="Apply", command = self.onApplyClick)
		self.buttonApply.pack(side = "right")
		self.buttonReset = self.tk.Button(text="Reset", command = self.onResetClick)
		self.buttonReset.pack(side = "right")

		self.setApplyResetEnabled(False)

		if f != "":
			self.onOpenClick(f)

		self.windowMain.after(50, self.weirdThreadHack)
		self.windowMain.mainloop()


	def updateTreeView(self, reset = False):
		children = self.treeView.get_children()
		if children != "()":
			for child in children:
				self.treeView.delete(child)

		if reset:
			self.plusX.resetFiles()

		index = 0
		font = "default"
		oddeven = "roweven"
		for archiveFile in self.plusX.fileList:
			if archiveFile[5]:
				font = "bold"
			else:
				font = "default"
			if index % 2 == 0:
				oddeven = "roweven"
			else:
				oddeven = "rowodd"
			self.treeView.insert(parent = "", index = "end", iid = index, values = (archiveFile[1], archiveFile[2], archiveFile[3], archiveFile[5]), tags=(font, oddeven))
			index += 1
		self.treeView.tag_configure("bold", font = ('Calibri', 11,'bold'))
		self.treeView.tag_configure("default", font = ('Calibri', 11,'normal'))



	def updateUIStatus(self):
		self.labelModifiedCount["text"] = "Pending file modifications: %d" % self.plusX.modifiedCount
		if self.plusX.modifiedCount > 0:
			self.setApplyResetEnabled(True)
		else:
			self.setApplyResetEnabled(False)


	def onTreeViewClick(self, event):
		if self.treeView.identify_region(event.x, event.y) == "cell":
			col = self.treeView.identify_column(event.x)
			if col == "#3":
				try:
					row = self.treeView.identify_row(event.y)
					#HACK: So, for some reason, on some systems, row 0 gets given a default index name. Yay.
					if row[0] == "I":
						print("Bad row ID detected (%s). Falling back to 0th row." % row)
						row = 0
					self.plusX.toggleFile(int(row))
					self.updateTreeView()
					self.updateUIStatus()
				except:
					#Clicked somewhere on the table that isn't a valid row. It happens. It's fine.
					pass

	def setApplyResetEnabled(self, enabled):
		if enabled:
			enabled = self.tk.NORMAL
		else:
			enabled = self.tk.DISABLED
		self.menuFile.entryconfigure("Apply", state = enabled)
		self.menuFile.entryconfigure("Reset", state = enabled)
		self.buttonApply["state"] = enabled
		self.buttonReset["state"] = enabled


	def onOpenClick(self, filename = ""):
		#TODO: Remember last folder?
		if filename == "":
			filename = self.filedialog.askopenfilename(title = "Select archive", filetypes = (("Zip archives","*.zip"),("Zip archives","*.zip")))
		if self.plusX.openArchive(filename):
			self.buttonOpen["text"] = "Current archive: %s" % self.plusX.filename.rsplit("/", 1)[1]
			self.updateTreeView(True)
			self.updateUIStatus()


	def onApplyClick(self):
		self.showProgressDialog()
		self.plusX.updateArchive(self.setProgress)
		self.updateTreeView()
		self.updateUIStatus()
		self.closeProgressDialog()


	def onResetClick(self):
		self.updateTreeView(True)
		self.updateUIStatus()


	def onAboutClick(self):
		self.messagebox.showinfo("About", self.plusX.applicationName + " " + self.plusX.applicationVersion + "\n\n" + self.plusX.applicationDescription + "\n\n" + self.plusX.applicationCopyright + "\n\n" + self.plusX.applicationWebsiteURI)


	def onInstructionsClick(self):
		self.messagebox.showinfo("Instructions", self.plusX.applicationInstructions)


	def showProgressDialog(self):
		self.windowProgress = self.tk.Tk()
		self.windowProgress.title("plus-x - busy")

		label = self.tk.Label(self.windowProgress, text = "Updating...")
		label.pack()
		self.progressBar = self.ttk.Progressbar(self.windowProgress, maximum = 100)
		self.progressBar.pack()


	def closeProgressDialog(self):
		self.windowProgress.destroy()
		self.windowProgress = None
		self.progressBar = None


	def setProgress(self, progress = 0.0):
		self.progressBar["value"] = progress * 100
		self.windowProgress.update()


	def onUIDestroy(self, a = None, b = None):
		if self.plusX.modifiedCount > 0:
			if self.messagebox.askquestion("Unapplied changes", "Are you sure to quit?") == "no":
				return

		self.plusX.teardown()
		#Destroy about dialog
		#Destroy instructions dialog
		self.windowMain.destroy()
		exit(0)


	def weirdThreadHack(self):
		#This is a gross way of making sure that we process interrupts when the application window doesn't have focus
		self.windowMain.after(50, self.weirdThreadHack)



def printUsage(shouldQuit = True):
	p = PlusX()
	p.displayVersion()
	print("%s" % (p.applicationCopyright))
	print("%s" % (p.applicationDescription))
	print("")
	print("Non-interactive usage: python plus-x.py file.zip [+x|-x] [files]")
	print("")
	print("  +x  add executable permissions to specified files")
	print("  -x  remove executable permissions from specified files")
	print("")
	print("Interactive interface options:")
	for ui in forceArgs:
		print("  %s\tForce usage of %s user interface" % (ui, forceArgs[ui].__name__.replace("PlusX", "").replace("UI", "")))
	print("")
	print("  Note that a ZIP archive must also be passed to plus-x when using --force-cli")
	if shouldQuit:
		sys.exit(0)


def showUIChoice(uiList):
	while True:
		print("\nAvailable interface options:")
		for i in range(len(uiList)):
			print("%d \tForce usage of %s user interface" % (i + 1, uiList[i].__name__.replace("PlusX", "").replace("UI", "")))
		print("q \tQuit\n")
		userInput = ""
		if sys.version_info[0] < 3:
			userInput = raw_input("> ")
		else:
			userInput = input("> ")
		if userInput.lower() == "q":
			sys.exit(0)

		isNumeric = False
		if sys.version_info[0] < 3:
			isNumeric = userInput.isdigit()
		else:
			isNumeric = userInput.isnumeric()
		if isNumeric:
			userInput = int(userInput) - 1
			if userInput >= 0 and userInput < len(uiList):
				return uiList[userInput]
		print("Invalid option.")


forceArgs = {
		"--force-gtk": PlusXGTKUI,
		"--force-tk": PlusXTKUI,
		"--force-cli": PlusXCLI,
}

if "--help" in sys.argv:
	printUsage(True)

gladeFilePath = "plus-x_ui.glade"
gladeFileExists = os.path.isfile(gladeFilePath)
if not gladeFileExists:
	runningPath = os.path.abspath(os.path.dirname(__file__))
	print("Changing working directory to %s" % runningPath)
	os.chdir(runningPath)
	gladeFileExists = os.path.isfile(gladeFilePath)
	if not gladeFileExists:
		print("Unable to find %s. GTK UI can not be used." % gladeFilePath)

p = None
for arg in forceArgs:
	if arg in sys.argv:
		print("Forcing specified UI")
		p = forceArgs[arg]
		sys.argv.remove(arg)

isTTY = sys.stdin.isatty()
if isTTY:
	try:
		import psutil
		if psutil.Process(os.getpid()).parent().name().lower() == "explorer.exe":
			#Apparently Explorer counts as a tty
			isTTY = False
	except:
		#If psutil isn't available, or we can't get the process name, then just assume that we're not running form a command line if we're on Windows and haven't provided an argument
		if os.name == "nt":
			if len(sys.argv) == 0:
				isTTY = False

if p == None and isTTY:
	p = PlusXCLI

if p == None:
	if "gi" in sys.modules and gladeFileExists:
		p = PlusXGTKUI
	elif "tkinter" in sys.modules or "Tkinter" in sys.modules:
		p = PlusXTKUI
	else:
		print("Couldn't show GUI.")
		p = PlusXCLI

f = None
if len(sys.argv) > 1:
	if sys.argv[1].endswith(".zip"):
		if os.path.isfile(sys.argv[1]):
			f = sys.argv[1]
if not f == None:
	if "-x" in sys.argv or "+x" in sys.argv:
		p = PlusXCL
if f == None and p == PlusXCLI:
	availableUIs = []
	if "gi" in sys.modules and gladeFileExists:
		availableUIs.append(PlusXGTKUI)
	if "tkinter" in sys.modules or "Tkinter" in sys.modules:
		availableUIs.append(PlusXTKUI)
	if len(availableUIs) > 0:
		print("No archive specified. Command line interfaces are unavailable.")
		print("Run with --help for instructions or select a graphical interface.")
		p = showUIChoice(availableUIs)

p(f)
