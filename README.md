# plus-x

A tool for managing UNIX executable permissions on zipped applications.

Typically, a filesystem requires an executable file to be flagged as such before the operating system can run it.  The most common implementation is part of what is typically referred to as "traditional Unix permissions." Unlike other modern (and many historic) operating systems, this is not supported by Windows or its default filesystems, meaning that information gets lost when extracted on Windows and won't be included when files are compressed on Windows.

Internally, the ZIP file format is capable of tracking Unix style executable permissions (used by Linux and MacOS). plus-x allows those permissions to be set after compressing so that users won't have to manually set that themselves.


plus-x is primarily aimed at Windows-based developers, but will run on Linux and MacOS. It can also be used to inspect executable permissions within ZIP files, which many archive utilities don't show.

In some cases, game downloader/launcher clients will set executable permissions for useres. plus-x is primarily intended to be useful to developers who are shipping applications that will be downloaded and manually extracted by users.


## Installation

plus-x requires [Python](https://www.python.org/downloads/), and is compatible with versions 2.7 and 3.7+. Python 2.7 is currently "end of life," but will remain supported so long as that's trivial.

plus-x comes with two graphical UIs: a GTK based GUI, and a fallback Tcl/Tk based GUI. Tcl/Tk is provided as part of a typical Python install. In order to use the GTK based GUI, you will need to have [pygobject](https://pygobject.readthedocs.io/) installed.


## Interactive usage

To run, double click on plus-x.py if Python file associations have been set up, or cd to the plus-x folder and run the following command in a terminal:

  python plus-x.py


plus-x includes several graphical and command line UIs. Launch arguments are provided as a way of forcing a particular UI to be used:

  --force-gtk
  --force-tk
  --force-cli


Using plus-x via a graphical interface is simple and straightforward:

  1. Open a Zip archive by clicking on the Open archive button or selecting Open from the File menu.
  2. Locate binaries within the archive that should be executable and click in the executable column.
  3. Click the Apply button or select Apply from the File menu to update the archive contents.
  4. Ship your application with peace of mind that your Linux users will be able to run it without fiddling with permissions.


The interactive command line interface included with plus-x includes menus with instructions, but a typical workflow looks like this:

  1. Specify a Zip archive when running plus-x.py from a command line.
  2. At the main menu, type `l` and then Enter to list the archve contents.
  3. At the main menu, type `+` or `-` and then Enter to add or remove permissions.
  4. At the Add or Remove prompts, type the path of each file to be modified followed by Enter.
  5. At the Add or Remove prompts, press Enter to return to the main menu.
  6. At the main menu, type `c` to apply any changes.
  7. Ship your application with peace of mind that your Linux users will be able to run it without fiddling with permissions.


## Non-interactive usage

plus-x can be invoked in non-interactive mode via the following syntax:

    python plus-x.py file.zip [+x|-x] [files]

The +x and -x switches are mutually exclusive, and add or remove executable permissions respectively. If both are present, +x will take precedence. If neither are present, plus-x will drop into the interactive command line interface.

Any number of files can be given, separated by a space. All files must include their path within the archive.


## Contributing

Bug reports and contributions can be made via the [plus-x GitLab repository](https://gitlab.com/Cheeseness/plus-x).


## History

v0.2 - Command line interfaces
v0.1 - Initial release

## Credits

plus-x was created through the efforts of, and is copyright to the following contributors:

  Josh "Cheeseness" Bush


## Licence

plus-x is made available under the MIT licence. Please see LICENCE file for full terms.
